# Helper methods defined here can be accessed in any controller or view in the application

module StewardAlfred
  class App
    module TransacaoHelper
      # def simple_helper_method
      # ...
      # end
    end

    helpers TransacaoHelper
  end
end
