# frozen_string_literal: true

# main module reference
module StewardAlfred
  # main app class reference
  class App
    # contahelbper module for app
    module ContaHelper
      # def simple_helper_method
      # ...
      # end
    end

    helpers ContaHelper
  end
end
